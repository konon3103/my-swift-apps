import UIKit
import Foundation

enum WeatherIconManager: String {
    case clearDay = "clear-day"
    case clearNight = "clear-night"
    case rain = "rain"
    case snow = "snow"
    case wind = "wind"
    case cloudy = "cloudy"
    case partyCloudyDay = "partly-cloudy-day"
    case partyCloudyNight = "partly-cloudy-night"
    case thunderstorm = "thunderstorm"
    
    init(rawValue: String){
        switch rawValue {
        case "clear-day":
            self = .clearDay
        case "clear-night":
            self = .clearDay
        case "rain":
            self = .rain
        case "snow":
            self = .snow
        case "wind":
            self = .wind
        case "cloudy":
            self = .cloudy
        case "partly-cloudy-day":
            self = .partyCloudyDay
        case "partly-cloudy-night":
            self = .partyCloudyDay
        case "thunderstrom":
            self = .thunderstorm
        default:
            self = .partyCloudyDay
        }
    }
}

extension WeatherIconManager {
    var image: UIImage{
        return UIImage(named: self.rawValue)!
    }
}
