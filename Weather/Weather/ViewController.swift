import CoreLocation
import UIKit

class ViewController: UIViewController, CLLocationManagerDelegate {
    
    
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var feelsLikeTemperature: UILabel!
    @IBOutlet weak var temperature: UILabel!
    @IBOutlet weak var weatherIcon: UIImageView!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var atmosphericPressureLabel: UILabel!
    
    let locationManager = CLLocationManager()
    var latitudeMain: Double = 0.0
    var longitudeMain: Double = 0.0
    
    lazy var weatherManager = APIWeatherManager(apiKey: "7df4689194bed6895ded1309298cf585")
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let coordinates = Coordinates(latitude: 53.93954, longitude: 27.60043)
        
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        
        weatherManager.fetchCurrentWeatherWith(coordinates: coordinates) { (result) in
            switch result{
            case .Success(let currentWeather):
                self.update(currentWeather: currentWeather)
            case .Error(let error as NSError):
                let alert = UIAlertController.init(title: "Unable to get data", message: "\(error.localizedDescription)", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                alert.addAction(okAction)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation :CLLocation = locations[0] as CLLocation
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        latitudeMain = userLocation.coordinate.latitude
        longitudeMain = userLocation.coordinate.longitude
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(userLocation) { (placemarks, error) in
            if (error != nil){
                print("error in reverseGeocode")
            }
            let placemark = placemarks! as [CLPlacemark]

            if placemark.count>0{
                let placemark = placemarks![0]
                print(placemark)
                self.locationLabel.text = placemark.name
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error \(error)")
    }
   

    func update(currentWeather: CurrentWeather){
        feelsLikeTemperature.text = "Feels like: \(Int(5/9*(currentWeather.feelsLikeTemperature-32)))˚"
        temperature.text = "\(Int(5/9*(currentWeather.temperature-32)))˚"
        weatherIcon.image = currentWeather.icon
        humidityLabel.text = "\(Int(currentWeather.humidity * 100))%"
        atmosphericPressureLabel.text = "\(Int(currentWeather.pressure * 0.750062)) mm"
    }

}


