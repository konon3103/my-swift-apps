import UIKit
import Foundation

struct CurrentWeather {
    let temperature: Double
    let feelsLikeTemperature: Double
    let humidity: Double
    let pressure: Double
    let icon: UIImage
}

extension CurrentWeather: JSONDecodable{
    init?(json: [String : AnyObject]) {
        guard let temperature = json["temperature"] as? Double,
            let feelsLikeTemperature = json["apparentTemperature"] as? Double,
            let humidity = json["humidity"] as? Double,
            let pressure = json["pressure"] as? Double,
            let iconString = json["icon"] as? String else {
                return nil
        }
        let icon = WeatherIconManager(rawValue: iconString).image
        self.temperature = temperature
        self.feelsLikeTemperature = feelsLikeTemperature
        self.humidity = humidity
        self.pressure = pressure
        self.icon = icon
    }
}
